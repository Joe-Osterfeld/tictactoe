﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToe.Models
{
    public class GameboardModel
    {
        public List<List<int>> Board { get; set; }

        public int PlayerMove { get; set; }

        public bool InvalidSelection { get; set; }

        public int Winner { get; set; }
    }
}