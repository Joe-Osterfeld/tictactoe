﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToe.Models
{
    public class TicTacToeCalculator
    {
        public GameboardModel CalculateWinner(GameboardModel gameboard)
        {
            int currentRow = 0;
            foreach (List<int> row in gameboard.Board)
            {
                int currentColumn = 0;
                foreach (int cell in row)
                {
                    List<string> commonNeighbors = GetCommonNeighbors(gameboard.Board, gameboard.Board[currentRow][currentColumn], currentRow, currentColumn);
                    foreach (string commonNeighbor in commonNeighbors)
                    {
                        if (gameboard.Board[currentRow][currentColumn] > 0 && commonNeighbor == "TL" && currentRow == 2 && currentColumn == 2 && gameboard.Board[0][0] == gameboard.Board[2][2])
                        {
                            gameboard.Winner = gameboard.Board[currentRow][currentColumn];
                            return gameboard;
                        }
                        else if (gameboard.Board[currentRow][currentColumn] > 0 && commonNeighbor == "T" && currentRow == 2 && gameboard.Board[0][currentColumn] == gameboard.Board[currentRow][currentColumn])
                        {
                            gameboard.Winner = gameboard.Board[currentRow][currentColumn];
                            return gameboard;
                        }
                        else if (gameboard.Board[currentRow][currentColumn] > 0 && commonNeighbor == "TR" && currentRow == 2 && currentColumn == 0 && gameboard.Board[0][2] == gameboard.Board[2][0])
                        {
                            gameboard.Winner = gameboard.Board[currentRow][currentColumn];
                            return gameboard;
                        }
                        else if (gameboard.Board[currentRow][currentColumn] > 0 && commonNeighbor == "L" && currentColumn == 2 && gameboard.Board[currentRow][0] == gameboard.Board[currentRow][currentColumn])
                        {
                            gameboard.Winner = gameboard.Board[currentRow][currentColumn];
                            return gameboard;
                        }
                    }
                    currentColumn++;
                }
                currentRow++;
            }
            return gameboard;
        }

        public bool IsGameADraw(List<List<int>> board)
        {
            int possibleWins = 0;

            int currentRow = 0;
            foreach (List<int> row in board)
            {
                int currentColumn = 0;
                foreach (int cell in row)
                {
                    if (currentRow == 0 && currentColumn == 0)
                    {
                        List<int> possibleWinLine = new List<int> { board[0][0], board[1][1], board[2][2] };
                        possibleWins += (IsLinePossibleWin(possibleWinLine)) ? 1 : 0;

                        List<int> possibleWinLine2 = new List<int> { board[0][0], board[1][0], board[2][0] };
                        possibleWins += (IsLinePossibleWin(possibleWinLine)) ? 1 : 0;
                    }
                    else if (currentRow == 0 && currentColumn != 0 && currentColumn != 2)
                    {
                        List<int> possibleWinLine = new List<int> { board[currentRow][currentColumn], board[currentRow + 1][currentColumn], board[currentRow + 2][currentColumn] };
                        possibleWins += (IsLinePossibleWin(possibleWinLine)) ? 1 : 0;
                    }
                    else if (currentRow == 0 && currentColumn == 2)
                    {
                        List<int> possibleWinLine = new List<int> { board[0][2], board[1][1], board[2][0] };
                        possibleWins += (IsLinePossibleWin(possibleWinLine)) ? 1 : 0;

                        List<int> possibleWinLine2 = new List<int> { board[0][2], board[1][2], board[2][2] };
                        possibleWins += (IsLinePossibleWin(possibleWinLine)) ? 1 : 0;
                    }
                    else if (currentColumn == 0)
                    {
                        List<int> possibleWinLine = new List<int> { board[currentRow][currentColumn], board[currentRow][currentColumn + 1], board[currentRow][currentColumn + 2] };
                        possibleWins += (IsLinePossibleWin(possibleWinLine)) ? 1 : 0;
                    }
                    currentColumn++;
                }
                currentRow++;
            }
            return (possibleWins == 0);
        }

        //Created separate method because code would be repeated in IsGameADraw Method
        public bool IsLinePossibleWin(List<int> line)
        {
            if (line.Contains(1) && !line.Contains(2))
            {
                return true;
            }
            else if (line.Contains(2) && !line.Contains(1))
            {
                return true;
            }
            else if(!line.Contains(1) && !line.Contains(2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Returns where a given cell has common neighbors 
        //  Strings:
        //      TL | T | TR
        //       L |...| R
        //      BL | B | BR

        public List<string> GetCommonNeighbors(List<List<int>> board, int player, int row, int column)
        {
            List<string> result = new List<string>();
            if (row == 0 && column == 0)
            {
                if (board[0][1] == player)
                {
                    result.Add("R");
                }
                if (board[1][1] == player)
                {
                    result.Add("BR");
                }
                if (board[1][0] == player)
                {
                    result.Add("B");
                }
            }
            else if (row == 0 && column == 1)
            {
                if (board[0][0] == player)
                {
                    result.Add("L");
                }
                if (board[1][0] == player)
                {
                    result.Add("BL");
                }
                if (board[1][1] == player)
                {
                    result.Add("B");
                }
                if (board[1][2] == player)
                {
                    result.Add("BR");
                }
                if (board[0][2] == player)
                {
                    result.Add("R");
                }
            }
            else if (row == 0 && column == 2)
            {
                if (board[0][1] == player)
                {
                    result.Add("L");
                }
                if (board[1][1] == player)
                {
                    result.Add("BL");
                }
                if (board[1][2] == player)
                {
                    result.Add("B");
                }
            }
            else if (row == 1 && column == 0)
            {
                if (board[0][0] == player)
                {
                    result.Add("T");
                }
                if (board[0][1] == player)
                {
                    result.Add("TR");
                }
                if (board[1][1] == player)
                {
                    result.Add("R");
                }
                if (board[2][1] == player)
                {
                    result.Add("BR");
                }
                if (board[2][0] == player)
                {
                    result.Add("B");
                }
            }
            else if (row == 1 && column == 2)
            {
                if (board[0][2] == player)
                {
                    result.Add("T");
                }
                if (board[0][1] == player)
                {
                    result.Add("TL");
                }
                if (board[1][1] == player)
                {
                    result.Add("L");
                }
                if (board[2][1] == player)
                {
                    result.Add("BL");
                }
                if (board[2][2] == player)
                {
                    result.Add("B");
                }
            }
            else if (row == 2 && column == 0)
            {
                if (board[1][0] == player)
                {
                    result.Add("T");
                }
                if (board[1][1] == player)
                {
                    result.Add("TR");
                }
                if (board[2][1] == player)
                {
                    result.Add("R");
                }
            }
            else if (row == 2 && column == 1)
            {
                if (board[2][0] == player)
                {
                    result.Add("L");
                }
                if (board[1][0] == player)
                {
                    result.Add("TL");
                }
                if (board[1][1] == player)
                {
                    result.Add("T");
                }
                if (board[1][2] == player)
                {
                    result.Add("TR");
                }
                if (board[2][2] == player)
                {
                    result.Add("R");
                }
            }
            else if (row == 2 && column == 2)
            {
                if (board[1][2] == player)
                {
                    result.Add("T");
                }
                if (board[1][1] == player)
                {
                    result.Add("TL");
                }
                if (board[2][1] == player)
                {
                    result.Add("L");
                }
            }
            else
            {
                if (board[0][0] == player)
                {
                    result.Add("TL");
                }
                if (board[0][1] == player)
                {
                    result.Add("T");
                }
                if (board[0][2] == player)
                {
                    result.Add("TR");
                }
                if (board[1][0] == player)
                {
                    result.Add("L");
                }
                if (board[1][2] == player)
                {
                    result.Add("R");
                }
                if (board[2][0] == player)
                {
                    result.Add("BL");
                }
                if (board[2][1] == player)
                {
                    result.Add("B");
                }
                if (board[2][2] == player)
                {
                    result.Add("BR");
                }
            }
            return result;
        }
    }
}