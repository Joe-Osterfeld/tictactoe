﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicTacToe.Models;

namespace TicTacToe.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<List<int>> boardModel = new List<List<int>>();
            //Create each empty row and add it to boardModel
            for (int i = 0; i < 3; i++)
            {
                List<int> row = new List<int>();
                for(int x = 0; x < 3; x++)
                {
                    row.Add(0);
                }
                boardModel.Add(row);
            }

            GameboardModel gameboard = new GameboardModel();
            gameboard.Board = boardModel;

            gameboard.PlayerMove = 1;

            Session["Gameboard"] = gameboard;
            gameboard = (GameboardModel)Session["Gameboard"];

            return View("Index", gameboard);
        }

        public ActionResult PlayerMove()
        {
            int row = Convert.ToInt32(Request.Params["Row"]) - 1;
            int column = Convert.ToInt32(Request.Params["Column"]) - 1;
            int playerMove = Convert.ToInt32(Request.Params["PlayerMove"]);

            GameboardModel model = (GameboardModel)Session["Gameboard"];
            Session["Gameboard"] = model;

            if(model.Board[row][column] == 0)
            {
                model.Board[row][column] = playerMove;
            }
            else
            {
                model.InvalidSelection = true;
                return View("Index", model);
            }

            //Use TicTacToeCalculator to see if there's a winner here
            GameboardModel newModel = new TicTacToeCalculator().CalculateWinner(model);

            //reset playerMove if valid selection was chosen
            newModel.PlayerMove = (playerMove == 1) ? 2 : 1;

            //Reset Model in Session
            newModel = (GameboardModel)Session["Gameboard"];
            Session["Gameboard"] = newModel;

            if(newModel.Winner == 0)
            {
                if(new TicTacToeCalculator().IsGameADraw(newModel.Board))
                {
                    return View("Draw", newModel);
                }
                return View("Index", newModel);
            }
            else
            {
                return View("GameOver", newModel);
            }
        }

        public void GameboardSession()
        {
            if(Session["Gameboard"] == null)
            {
                List<List<int>> boardModel = new List<List<int>>();
                //Create each empty row and add it to boardModel
                for (int i = 0; i < 3; i++)
                {
                    List<int> row = new List<int>();
                    for (int x = 0; x < 3; x++)
                    {
                        row.Add(0);
                    }
                    boardModel.Add(row);
                }

                GameboardModel gameboard = new GameboardModel();
                gameboard.Board = boardModel;

                Session["Gameboard"] = gameboard.Board;
                gameboard = (GameboardModel)Session["Gameboard"];
            }
        }
    }
}