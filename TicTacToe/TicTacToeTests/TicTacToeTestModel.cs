﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicTacToe.Models;
using System.Collections.Generic;

namespace TicTacToeTests
{
    [TestClass]
    public class TicTacToeTests
    {
        [TestMethod]
        public void CalculateWinner()
        {
            GameboardModel game = new GameboardModel();

            //First Diagonal
            List<List<int>> board = new List<List<int>>
            {
                new List<int> { 2, 1, 1 },
                new List<int> { 2, 1, 1 },
                new List<int> { 1, 2, 0 }
            };
            game.Board = board;

            TicTacToeCalculator calculator = new TicTacToeCalculator();
            Assert.AreEqual(1, calculator.CalculateWinner(game).Winner);

            //2nd & Final Diagonal
            List<List<int>> board1 = new List<List<int>>
            {
                new List<int> { 2, 1, 1 },
                new List<int> { 2, 2, 1 },
                new List<int> { 1, 2, 2 }
            };
            game.Board = board1;

            Assert.AreEqual(2, calculator.CalculateWinner(game).Winner);

            //Vertical1
            List<List<int>> board2 = new List<List<int>>
            {
                new List<int> { 2, 1, 1 },
                new List<int> { 2, 2, 1 },
                new List<int> { 2, 2, 0 }
            };
            game.Board = board2;

            Assert.AreEqual(2, calculator.CalculateWinner(game).Winner);

            //Vertical2
            List<List<int>> board3 = new List<List<int>>
            {
                new List<int> { 2, 1, 1 },
                new List<int> { 2, 1, 1 },
                new List<int> { 0, 1, 0 }
            };
            game.Board = board3;

            Assert.AreEqual(1, calculator.CalculateWinner(game).Winner);

            //Vertical3
            List<List<int>> board4 = new List<List<int>>
            {
                new List<int> { 2, 1, 2 },
                new List<int> { 1, 1, 2 },
                new List<int> { 0, 2, 2 }
            };
            game.Board = board4;

            Assert.AreEqual(2, calculator.CalculateWinner(game).Winner);

            //Horizontal 1
            List<List<int>> board5 = new List<List<int>>
            {
                new List<int> { 2, 2, 2 },
                new List<int> { 1, 1, 2 },
                new List<int> { 0, 2, 2 }
            };
            game.Board = board5;

            Assert.AreEqual(2, calculator.CalculateWinner(game).Winner);

            //Horizontal 2
            List<List<int>> board6 = new List<List<int>>
            {
                new List<int> { 1, 2, 2 },
                new List<int> { 1, 1, 1 },
                new List<int> { 0, 2, 2 }
            };
            game.Board = board6;

            Assert.AreEqual(1, calculator.CalculateWinner(game).Winner);

            //Horizontal 3
            List<List<int>> board7 = new List<List<int>>
            {
                new List<int> { 1, 2, 2 },
                new List<int> { 1, 0, 1 },
                new List<int> { 2, 2, 2 }
            };
            game.Board = board7;

            Assert.AreEqual(2, calculator.CalculateWinner(game).Winner);
        }

        [TestMethod]
        public void IsGameADraw()
        {
            GameboardModel game = new GameboardModel();
            TicTacToeCalculator calculator = new TicTacToeCalculator();

            List<List<int>> board = new List<List<int>>
            {
                new List<int> { 2, 1, 2 },
                new List<int> { 2, 1, 1 },
                new List<int> { 1, 2, 0 }
            };
            game.Board = board;

            Assert.IsTrue(calculator.IsGameADraw(game.Board));

            List<List<int>> board2 = new List<List<int>>
            {
                new List<int> { 1, 2, 1 },
                new List<int> { 1, 2, 2 },
                new List<int> { 2, 1, 0 }
            };
            game.Board = board2;

            Assert.IsTrue(calculator.IsGameADraw(game.Board));

            List<List<int>> board3 = new List<List<int>>
            {
                new List<int> { 2, 1, 2 },
                new List<int> { 2, 1, 1 },
                new List<int> { 1, 2, 0 }
            };
            game.Board = board3;

            Assert.IsTrue(calculator.IsGameADraw(game.Board));

            List<List<int>> board4 = new List<List<int>>
            {
                new List<int> { 1, 0, 0 },
                new List<int> { 0, 2, 0 },
                new List<int> { 0, 0, 0 }
            };
            game.Board = board4;

            Assert.IsFalse(calculator.IsGameADraw(game.Board));

            List<List<int>> board5 = new List<List<int>>
            {
                new List<int> { 1, 2, 2 },
                new List<int> { 0, 1, 0 },
                new List<int> { 0, 2, 0 }
            };
            game.Board = board5;

            Assert.IsFalse(calculator.IsGameADraw(game.Board));

            //Case I found was wrong with visual testing
            List<List<int>> board6 = new List<List<int>>
            {
                new List<int> { 1, 0, 0 },
                new List<int> { 2, 1, 1 },
                new List<int> { 1, 2, 2 }
            };
            game.Board = board6;

            Assert.IsFalse(calculator.IsGameADraw(game.Board)); //Works now?
        }

        [TestMethod]
        public void IsLinePossibleWin()
        {
            TicTacToeCalculator calc = new TicTacToeCalculator();

            List<int> line1 = new List<int> { 1, 0, 1 };
            Assert.IsTrue(calc.IsLinePossibleWin(line1));

            List<int> line2 = new List<int> { 0, 0, 0 };
            Assert.IsTrue(calc.IsLinePossibleWin(line2));

            List<int> line3 = new List<int> { 2, 0, 2 };
            Assert.IsTrue(calc.IsLinePossibleWin(line3));

            List<int> line4 = new List<int> { 1, 2, 1 };
            Assert.IsFalse(calc.IsLinePossibleWin(line4));

            List<int> line5 = new List<int> { 1, 0, 2 };
            Assert.IsFalse(calc.IsLinePossibleWin(line5));
        }

        [TestMethod]
        public void GetCommonNeighbors()
        {
            GameboardModel game = new GameboardModel();
            TicTacToeCalculator calc = new TicTacToeCalculator();

            List<List<int>> board1 = new List<List<int>>
            {
                new List<int> { 2, 1, 2 },
                new List<int> { 2, 1, 1 },
                new List<int> { 1, 2, 0 }
            };
            game.Board = board1;

            Assert.AreEqual("TR", calc.GetCommonNeighbors(game.Board, 1, 2, 0)[0]);

            Assert.AreEqual("T", calc.GetCommonNeighbors(game.Board, 2, 1, 0)[0]);
            Assert.AreEqual("BR", calc.GetCommonNeighbors(game.Board, 2, 1, 0)[1]);

            Assert.AreEqual("TL", calc.GetCommonNeighbors(game.Board, 1, 1, 2)[0]);
            Assert.AreEqual("L", calc.GetCommonNeighbors(game.Board, 1, 1, 2)[1]);

            Assert.AreEqual("B", calc.GetCommonNeighbors(game.Board, 1, 0, 1)[0]);
            Assert.AreEqual("BR", calc.GetCommonNeighbors(game.Board, 1, 0, 1)[1]);

            Assert.AreEqual("T", calc.GetCommonNeighbors(game.Board, 1, 1, 1)[0]);
            Assert.AreEqual("R", calc.GetCommonNeighbors(game.Board, 1, 1, 1)[1]);
            Assert.AreEqual("BL", calc.GetCommonNeighbors(game.Board, 1, 1, 1)[2]);
        }
    }
}
